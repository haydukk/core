// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <boost/lexical_cast.hpp>
#include <map>
#include <string>

namespace game::formats {
    struct StructuredFile {
        std::map<std::string, std::string> values;

        std::string GetValue(const std::string &name, unsigned int index);

        std::string GetValue(const std::string &name) { return values[name]; }

        template <typename T>
        T GetValue(const std::string &name) {
            return boost::lexical_cast<T>(GetValue(name));
        }

        template <typename T>
        T GetValue(const std::string &name, unsigned int index) {
            return boost::lexical_cast<T>(GetValue(name, index));
        }
    };

    class StructuredFileReader {
       public:
        static StructuredFile Read(const std::string &path);
    };
}  // namespace game::formats