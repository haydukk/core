// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <cstdint>
#include <memory>

#include "../mysql/mysql.hpp"
#include "redis.hpp"

namespace core::cache {
    struct Account {
        uint32_t id;
        uint8_t empire;
    };

    class AccountCache {
       public:
        AccountCache(std::shared_ptr<Redis> redis,
                     std::shared_ptr<mysql::MySQL> database);
        virtual ~AccountCache();

        uint8_t GetEmpire(uint32_t id);
        void SetEmpire(uint32_t id, uint8_t empire);

       private:
        std::shared_ptr<Redis> _redis;
        std::shared_ptr<mysql::MySQL> _database;
    };
}  // namespace core::cache
