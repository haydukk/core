// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "column.hpp"

#include <utility>

namespace core::mysql::structures {
    Column::Column(std::string name, Type type)
        : _name(std::move(name)), _type(std::move(type)) {}

    const std::string &Column::GetName() const { return _name; }

    const std::string &Column::GetDefaultValue() const { return _defaultValue; }

    const Type &Column::GetType() const { return _type; }

    Column &Column::Default(const std::string &defaultValue) {
        _defaultValue = defaultValue;
        return *this;
    }
}  // namespace core::mysql::structures