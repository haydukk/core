/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "../migration.hpp"

#define MIGRATION(name)           \
    class name : public Migration \
    {                             \
       public:                    \
        name();                   \
    }

namespace core {
    namespace mysql {
        namespace migrations {
            MIGRATION(M20181013_234200_CreateAccounts);

            MIGRATION(M20181027_214100_CreatePlayers);

            MIGRATION(M20181027_224500_CreatePlayerData);

            MIGRATION(M20181027_225700_CreatePlayerEquipment);

            MIGRATION(M20181027_225900_CreatePlayerSkills);

            MIGRATION(M20181027_230100_CreatePlayerQuickSlots);
        } // namespace migrations
    } // namespace mysql
} // namespace core