// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include "../../../logger.hpp"

namespace core::scripting::angelscript::proxy {
    class Ref {
       public:
        Ref() : _refCount(1) {}

        void AddRef() { _refCount++; }

        void ReleaseRef() {
            _refCount--;
            if (_refCount <= 0) {
                delete this;
            }
        }

       private:
        int _refCount;
    };
}  // namespace core::scripting::angelscript::proxy